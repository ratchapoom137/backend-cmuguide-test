package dv.cmuguide.entity.dto

import javax.persistence.Entity

@Entity
data class Customer(
        override var email: String? = null,
        override var password: String? = null,
        var firstName: String? = null,
        var lastName: String? = null,
        var image: String? = null
) : User(email, password) {

}