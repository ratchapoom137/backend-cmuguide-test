package dv.cmuguide.entity.dto

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class Review(
        var comment: String? = null,
        var rate: String? = null,
        var shopId: Long? = null,
        var isDeleted: Boolean = false
) {
    @Id
    @GeneratedValue
    var id: Long? = null
    @ManyToOne
    var customer: Customer? = null
}