package dv.cmuguide.entity.dto

import javax.persistence.*

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
abstract class User(
        open var email: String? = null,
        open var password: String? = null
) {
    @Id
    @GeneratedValue
    var id: Long? = null
}