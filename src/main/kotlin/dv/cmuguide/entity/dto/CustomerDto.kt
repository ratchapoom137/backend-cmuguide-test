package dv.cmuguide.entity.dto

data class CustomerDto (
    var email: String? = null,
    var firstName: String? = null,
    var lastName: String? = null,
    var image: String? = null
)