package dv.cmuguide.service

import dv.cmuguide.entity.dto.Review

interface ReviewService {
    fun getReviewByUserId(id: Long): Review
//    fun getReviewByShopId(id: Long): Review
//    fun addReviewByUserId(review: Review): Review
//    fun editReviewByUserId(review: Review): Review
//    fun deleteReviewByReviewId(id: Long): Review
//    fun save(review: Review): Review
}