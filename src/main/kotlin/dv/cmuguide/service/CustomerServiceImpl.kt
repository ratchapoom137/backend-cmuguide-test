package dv.cmuguide.service

import dv.cmuguide.dao.CustomerDao
import dv.cmuguide.entity.dto.Customer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CustomerServiceImpl : CustomerService {
    override fun getUserById(id: Long): Customer {
        return customerDao.getUserById(id)
    }

    @Autowired
    lateinit var customerDao: CustomerDao
}