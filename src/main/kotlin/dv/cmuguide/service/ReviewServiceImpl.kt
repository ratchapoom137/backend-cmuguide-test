package dv.cmuguide.service

import dv.cmuguide.dao.ReviewDao
import dv.cmuguide.entity.dto.Review
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ReviewServiceImpl: ReviewService {
    override fun getReviewByUserId(id: Long): Review {
        return  reviewDao.getReviewByUserId(id)
    }

    @Autowired
    lateinit var reviewDao: ReviewDao
}