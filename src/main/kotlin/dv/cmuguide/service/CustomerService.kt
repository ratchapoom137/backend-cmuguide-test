package dv.cmuguide.service

import dv.cmuguide.entity.dto.Customer

interface CustomerService {
    fun getUserById(id: Long): Customer
//    fun editUser(customer: Customer): Customer
}