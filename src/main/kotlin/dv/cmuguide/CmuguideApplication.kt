package dv.cmuguide.cmuguide

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CmuguideApplication

fun main(args: Array<String>) {
    runApplication<CmuguideApplication>(*args)
}
