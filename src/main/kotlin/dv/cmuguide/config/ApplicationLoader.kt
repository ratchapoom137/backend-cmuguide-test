package dv.cmuguide.config

import dv.cmuguide.entity.dto.Customer
import dv.cmuguide.repository.CustomerRepository
import dv.cmuguide.repository.ReviewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
class ApplicationLoader : ApplicationRunner {
    @Autowired
    lateinit var customerRepository: CustomerRepository
    @Autowired
    lateinit var reviewRepository: ReviewRepository

    @Transactional
    override fun run(args: ApplicationArguments?) {
        var customer1 = customerRepository.save(Customer("poom@gmail.com", "1234", "rat", "cha", "eiei"))
        
    }
}