package dv.cmuguide.dao

import dv.cmuguide.entity.dto.Review

interface ReviewDao {
    fun getReviewByUserId(id: Long): Review


}