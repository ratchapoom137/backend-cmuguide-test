package dv.cmuguide.dao

import dv.cmuguide.entity.dto.Customer
import dv.cmuguide.repository.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class CustomerDaoDBImpl: CustomerDao {
    override fun getUserById(id: Long): Customer {
        return customerRepository.findById(id).orElse(null)
    }

    @Autowired
    lateinit var customerRepository: CustomerRepository
}