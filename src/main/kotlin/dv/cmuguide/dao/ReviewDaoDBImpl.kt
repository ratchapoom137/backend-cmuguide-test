package dv.cmuguide.dao

import dv.cmuguide.entity.dto.Review
import dv.cmuguide.repository.ReviewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ReviewDaoDBImpl: ReviewDao {
    override fun getReviewByUserId(id: Long): Review {
        return reviewRepository.findById(id).orElse(null)
    }

    @Autowired
    lateinit var reviewRepository: ReviewRepository
}