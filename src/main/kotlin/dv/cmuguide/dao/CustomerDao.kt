package dv.cmuguide.dao

import dv.cmuguide.entity.dto.Customer

interface CustomerDao {
    fun getUserById(id: Long): Customer

}