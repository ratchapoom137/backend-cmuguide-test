package dv.cmuguide.controller

import dv.cmuguide.entity.dto.Customer
import dv.cmuguide.service.CustomerService
import dv.cmuguide.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class CustomerController {
    @Autowired
    lateinit var customerService: CustomerService

    @GetMapping("/user/{userId}")
    fun getUserById(@PathVariable("userId") id: Long): ResponseEntity<Any> {
        val customer = customerService.getUserById(id)
        return ResponseEntity.ok(customer)
    }

//    @PutMapping("/user/edit")
//    fun editUser(@RequestBody customer: Customer): ResponseEntity<Any> {
//        val output = customerService.editUser(customer)
//        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
//        outputDto?.let { return ResponseEntity.ok(outputDto) }
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
//    }
}