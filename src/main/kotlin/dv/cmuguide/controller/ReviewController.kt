package dv.cmuguide.controller

import dv.cmuguide.entity.dto.Review
import dv.cmuguide.service.ReviewService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ReviewController {
    @Autowired
    lateinit var reviewService: ReviewService

    @GetMapping("/review/shop/{shopId}")
    fun getReviewByShopId(@PathVariable("shopId") id: Long): ResponseEntity<Any> {
        val output = reviewService.getReviewByUserId(id)
        return ResponseEntity.ok(output)
    }

//    @GetMapping("/review/user/{userId}")
//    fun getReviewByUserId(@PathVariable("userId") id: Long): ResponseEntity<Any> {
//        val output = reviewService.getReviewByShopId(id)
//        return ResponseEntity.ok(output)
//    }
//
//    @PostMapping("/review/add")
//    fun addReviewByUserId(@RequestBody review: Review): ResponseEntity<Any> {
//        val output = reviewService.save(review)
//        return ResponseEntity.ok(output)
//    }
}