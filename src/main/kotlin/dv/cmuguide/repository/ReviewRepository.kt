package dv.cmuguide.repository

import dv.cmuguide.entity.dto.Review
import org.springframework.data.repository.CrudRepository

interface ReviewRepository: CrudRepository<Review, Long> {

}