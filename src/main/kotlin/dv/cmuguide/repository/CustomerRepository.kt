package dv.cmuguide.repository

import dv.cmuguide.entity.dto.Customer
import org.springframework.data.repository.CrudRepository

interface CustomerRepository: CrudRepository<Customer, Long> {

}