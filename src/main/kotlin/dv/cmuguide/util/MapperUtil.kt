package dv.cmuguide.util

import dv.cmuguide.entity.dto.Customer
import dv.cmuguide.entity.dto.CustomerDto
import org.mapstruct.Mapper
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    fun mapCustomerDto(customer: Customer): CustomerDto

}